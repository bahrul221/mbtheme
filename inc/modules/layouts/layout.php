<?php

/**
 * Layout controllers
 * 
 */
final class MBTheme_Layout {

    private static $data;

    /**
     * Initialize
     * 
     * @access public
     */
    public static function init() {
	self::actions();
    }

    /**
     * Actions and Filters
     */
    private static function actions() {
	// set data after wp object is set up
	add_action( 'wp', array( 'MBTheme_layout', 'setData' ) );

	// filter site-content-area (container) class
	add_filter( 'mbtheme_layout_class', array( 'MBTheme_Layout', 'containerClass' ) );

	// add action to wrap .site-main
	add_action( 'mbtheme_before_site_main', array( 'MBTheme_Layout', 'open' ), 9999 );
	add_action( 'mbtheme_after_site_main', array( 'MBTheme_Layout', 'close' ), 1 );
    }

    /**
     * Set layout data
     */
    public static function setData() {
	$default = array(
	    'container' => 'default',
	    'sidebar_pos' => '',
	    'sidebar_right' => 'mbtheme-default-right',
	    'sidebar_left' => 'mbtheme-default-left',
	);
	$data = array();
	// set data here
	$data = MBTheme_Layout_Settings_Default::getInstance()->getValues( 'default' );

	if ( is_home() ) {
	    // default homepage
	    $data = MBTheme_Layout_Settings_Default::getInstance()->getValues( 'archive_post' );
	}
	if ( get_post_type() ) {
	    // get from setting options
	    $post_type = get_post_type();
	    $data = MBTheme_Layout_Settings_Default::getInstance()->getValues( 'single_' . $post_type );
	    if ( is_archive() ) {
		$data = MBTheme_Layout_Settings_Default::getInstance()->getValues( 'archive_' . $post_type );
	    }elseif ( is_singular() ) {
		// check meta box
		$post_layout = MBTheme_Layouts_Posts::getPostLayout( get_queried_object_id() );
		$data = ($post_layout) ? $post_layout : $data;
	    }
	}
	if ( is_search() ) {
	    // search result
	    $data = MBTheme_Layout_Settings_Default::getInstance()->getValues( '_search_result' );
	} elseif ( is_404() ) {
	    // not found page
	    $data = MBTheme_Layout_Settings_Default::getInstance()->getValues( '_404' );
	}
	// merge data to default
	self::$data = array_merge( $default, $data );
    }

    /**
     * Filter container classes
     */
    public static function containerClass( $classes ) {
	switch ( self::$data[ 'container' ] ) {
	    case 'large':
		$classes[] = 'container-large';
		break;
	    case 'fluid':
		$classes[] = 'container-fluid';
		break;
	    case 'full_width':
		$classes[] = 'container-full-width';
		break;
	}
	return $classes;
    }

    /**
     * Open layout elements
     */
    public static function open() {
	// set classes for layout-row
	switch ( self::$data[ 'sidebar_pos' ] ) {
	    case 'left':
		$class = 'layout-has-left';
		break;
	    case 'right':
		$class = 'layout-has-right';
		break;
	    default:
		$class = '';
		break;
	}

	// open .layout-row and .layout-main
	echo '<div class="layout-row ' . $class . '"><div class="layout-main">';
    }

    /**
     * Close layout elements
     */
    public static function close() {
	// close .layout-main
	echo '</div>';

	// display side area
	self::sideArea();

	// close .layout-row
	echo '</div>';
    }

    /**
     * Display side area
     */
    private static function sideArea() {
	// if has left side
	if ( self::$data[ 'sidebar_pos' ] === 'left' ) {
	    ?>
	    <div class="layout-side layout-left">
		<?php self::displaySidebar( self::$data[ 'sidebar_left' ] ); ?>
	    </div>
	    <?php
	}
	// if has right side
	if ( self::$data[ 'sidebar_pos' ] === 'right' ) {
	    ?>
	    <div class="layout-side layout-right">
		<?php self::displaySidebar( self::$data[ 'sidebar_right' ] ); ?>
	    </div>
	    <?php
	}
    }

    /**
     * Display the widgets
     */
    public static function displaySidebar( $sidebar_id ) {
	if ( is_dynamic_sidebar( $sidebar_id ) ) {
	    ?>
	    <div class="site-sidebar">
		<?php dynamic_sidebar( $sidebar_id ); ?>
	    </div>
	    <?php
	}
    }

}
