<?php

final class MBTheme_Layouts_Posts {

    /**
     * @var string Meta Box ID.
     */
    protected static $metabox_id = 'layout';

    /**
     * @var string key.
     */
    protected static $key = 'mbtheme_layout_post_types';

    /**
     * Initialization.
     */
    public static function init() {
	self::actions();
    }

    /**
     * Actions and Filters.
     */
    private static function actions() {
	add_action( 'init', array( __CLASS__, 'addMetaBox' ) );
	// save settings on admin post.
	add_action( "admin_post_" . self::$key, array( __CLASS__, 'save' ) );
    }

    /**
     * Create Meta Box.
     */
    public static function addMetaBox() {
	$args = array(
	    'screen' => self::getValues(),
	    'container_class' => 'mbtheme-layouts-fieldset',
	    'save_as_array' => true,
	);
	$form = array(
	    'fields' => MBTheme_Layout_Settings_Default::getFields( 'layout' )
	);
	MBDL_Posts::addMetaBox( self::$metabox_id, 'Layout', '', $args, $form );
    }

    /**
     * Display settings page.
     */
    public static function displaySetting() {
	?>
	<div class="stuffbox" style="width: 500px; max-width: 100%;">
	    <div class="inside">
		<h3>Display Meta Box</h3>
		<form method="POST" action="admin-post.php">
		    <input type="hidden" name="action" value="<?php echo self::$key; ?>">
		    <input type="hidden" name="<?php echo esc_attr( self::$key ); ?>" value="" >
		    <?php
		    $values = self::getValues();
		    $post_types = get_post_types( array( 'public' => true ), 'objects' );
		    foreach ( $post_types as $key => $post_type ) {
			if ( $post_type->name === 'attachment' ) {
			    continue;
			}
			$name = esc_attr( self::$key . '[]' );
			$value = esc_attr( $post_type->name );
			$checked = (in_array( $post_type->name, $values )) ? 'checked=""' : '';
			$label = sprintf( "%s (%s)", $post_type->label, $post_type->name );
			$input = sprintf( '<input type="checkbox" name="%s" value="%s" %s> %s', $name, $value, $checked, $label );
			echo "<p><label>$input</label></p>";
		    }
		    wp_nonce_field( self::$key, self::$key . "_nonce" );
		    submit_button( 'Save Settings' );
		    ?>
		</form>
	    </div>
	</div>
	<?php
    }

    /**
     * Get list of post types that will display meta box.
     * 
     * @return array An array of post type.
     */
    public static function getValues() {
	$values = get_option( self::$key, array() );
	return (empty( $values )) ? array() : $values;
    }

    /**
     * 
     */
    public static function getPostLayout( $post_id ) {
	$post_type = get_post_type( $post_id );
	if ( !in_array( $post_type, (array) self::getValues() ) ) {
	    return false;
	}
	$value = MBDL_Posts::getMetaValue( self::$metabox_id, $post_id );
	return empty( $value ) ? false : $value;
    }

    /**
     * Save the settings.
     */
    public static function save() {
	// begin validation
	if ( !current_user_can( 'manage_options' ) ) {
	    wp_die( 'You are not allowed to be on this page.' );
	}
	check_admin_referer( self::$key, self::$key . "_nonce" );
	// end validation

	if ( isset( $_POST[ self::$key ] ) ) {
	    update_option( self::$key, $_POST[ self::$key ] );
	    // set error settings
	    add_settings_error( 'general', 'settings_updated', 'Settings Updated.', 'updated' );
	} else {
	    add_settings_error( 'general', 'settings_updated', 'Failed.', 'error' );
	}

	set_transient( 'settings_errors', get_settings_errors(), 30 );

	// redirect
	$goback = add_query_arg( 'settings-updated', 'true', wp_get_referer() );
	wp_redirect( $goback );
	exit;
    }

}
