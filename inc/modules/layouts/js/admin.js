( function ( $ ) {
    $( function ( ) {

	$( '.mbtheme-layouts-fieldset [name*="[sidebar_pos]"]' ).change( function () {
	    var fieldset = $( this ).closest( '.mbtheme-layouts-fieldset' );
	    var left = $( '[name*="sidebar_left"]', fieldset ).closest( '.mbdl-af-group' );
	    var right = $( '[name*="sidebar_right"]', fieldset ).closest( '.mbdl-af-group' );
	    switch ( $( this ).val() ) {
		case 'left':
		    left.fadeIn( 300 );
		    right.hide();
		    break;
		case 'right':
		    right.fadeIn( 300 );
		    left.hide();
		    break;
		default:
		    right.hide();
		    left.hide();

	    }
	} );
	$( '.mbtheme-layouts-fieldset [name*="[sidebar_pos]"]' ).change();

	$( '.mbtheme-layouts-fieldset [name*="[default]"]' ).not( '[type="hidden"]' ).change( function () {
	    var fieldset = $( this ).closest( '.mbtheme-layouts-fieldset' );
	    var tr = $( this ).closest( '.mbdl-af-group' );
	    var left = $( '[name*="sidebar_left"]', fieldset ).closest( '.mbdl-af-group' ).hide( 0 );
	    var right = $( '[name*="sidebar_right"]', fieldset ).closest( '.mbdl-af-group' ).show( 0 );
	    if ( $( this ).prop( 'checked' ) ) {
		tr.siblings().hide( 0 );
	    } else {
		tr.siblings().not( left ).not( right ).show( 0, function () {
		    $( '[name*="[sidebar_pos]"]', fieldset ).change();
		} );
	    }
	} );
	$( '.mbtheme-layouts-fieldset [name*="[default]"]' ).change();

    } );
}( jQuery ) );