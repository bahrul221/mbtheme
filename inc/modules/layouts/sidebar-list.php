<?php

class MBTheme_Sidebar_List extends WP_List_Table {

    protected $example_data = array(
	array( 'name' => 'Sidebar 1', 'id' => 'sidebar_1', 'description' => 'Description 1', ),
	array( 'name' => 'Sidebar 2', 'id' => 'sidebar_2', 'description' => 'Description 2', ),
	array( 'name' => 'Sidebar 3', 'id' => 'sidebar_3', 'description' => 'Description 3', ),
    );

    public function get_columns() {
	$columns = array(
	    'name' => 'Name',
	    'id' => 'Key',
	    'description' => 'Description',
	);
	return $columns;
    }

    /**
     * Get sidebar list.
     */
    protected function get_list() {
	$sidebars_ = MBTheme_Sidebars::getList();
	// id should inside array list
	$sidebars = array();
	foreach ( $sidebars_ as $key => $sidebar ) {
	    $sidebar[ 'id' ] = $key;
	    $sidebars[] = $sidebar;
	}
	return $sidebars;
    }

    function prepare_items() {
	$columns = $this->get_columns();
	$hidden = array();
	$sortable = array();
	$this->_column_headers = array( $columns, $hidden, $sortable );
	$this->items = $this->get_list();
    }

    protected function column_default( $item, $column_name ) {
	switch ( $column_name ) {
	    case 'name':
	    case 'description':
	    case 'id':
		return $item[ $column_name ];
	    default:
		return print_r( $item, true ); //Show the whole array for troubleshooting purposes
	}
    }

    protected function column_name( $item ) {

	$page = $_REQUEST[ 'page' ];
	$tab = $_REQUEST[ 'tab' ];
	$parent_file = get_current_screen()->parent_file;

	// Build edit row action.
	$edit_query_args = array(
	    'page' => $page,
	    'tab' => $tab,
	    'action' => 'edit_sidebar',
	    'sidebar_id' => $item[ 'id' ],
	);
	$actions[ 'edit' ] = sprintf(
	'<a href="%1$s">%2$s</a>', esc_url( add_query_arg( $edit_query_args, $parent_file ) ), 'Edit'
	);

	// Build edit row action.
	$delete_query_args = array(
	    'page' => $page,
	    'tab' => $tab,
	    'action' => 'delete_sidebar',
	    'sidebar_id' => $item[ 'id' ],
	);
	$onclick = 'onclick="return confirm(\'Are you sure?\')"';
	$actions[ 'delete' ] = sprintf(
	'<a href="%1$s" ' . $onclick . '>%2$s</a>', esc_url( wp_nonce_url( add_query_arg( $delete_query_args, $parent_file ), 'mbtheme_sidebar_delete_' . $item[ 'id' ] ) ), 'Delete'
	);

	return $item[ 'name' ] . $this->row_actions( $actions );
    }

}
