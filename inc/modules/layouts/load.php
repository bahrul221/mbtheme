<?php

if ( !class_exists( 'WP_List_Table' ) && is_admin() ) {
    require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}

$dir = dirname( __FILE__ );

include_once $dir . '/settings.php';
include_once $dir . '/settings-default.php';
include_once $dir . '/settings-sidebars.php';
include_once $dir . '/sidebar-list.php';
include_once $dir . '/settings-posts.php';
include_once $dir . '/layout.php';

MBTheme_Layout_Settings::getInstance();
MBTheme_Layout_Settings_Default::getInstance();
MBTheme_Sidebars::init();
MBTheme_Layouts_Posts::init();
MBTheme_Layout::init();