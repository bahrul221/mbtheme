<?php

/**
 * Layout settings page.
 */
class MBTheme_Layout_Settings extends MBDL_Admin_Page {

    /**
     * @access private
     * @var object Instance of current class
     */
    private static $instance;

    /**
     * Private construct because of singleton
     * @access private
     */
    private function __construct() {
	
    }

    /**
     * Private because of singleton
     * @access private
     */
    private function __clone() {
	
    }

    /**
     * Private because of singleton
     * @access private
     */
    private function __wakeup() {
	
    }

    /**
     * Returns instance of this class
     * 
     * @access public
     * @return object instance of this class
     */
    public static function getInstance() {
	if ( self::$instance === null ) {
	    self::$instance = new self();
	    self::$instance->init();
	}
	return self::$instance;
    }

    /**
     * Initialize class
     * 
     * @access protected
     */
    protected function init() {
	// set menu args
	parent::menuArgs( array(
	    'parent_slug' => 'themes.php',
	    'page_title' => _x( 'Layouts', 'admin page title', 'mbtheme' ),
	    'menu_title' => _x( 'Layouts', 'admin menu title', 'mbtheme' ),
	    'menu_slug' => 'mbtheme_layouts',
	) );

	// set tabs
	$this->tabs = array(
	    'default' => _x( 'Default', 'tab title', 'mbtheme' ),
	    'sidebars' => _x( 'Sidebars', 'tab title', 'mbtheme' ),
	    'metabox' => _x( 'Meta Box', 'tab title', 'mbtheme' ),
	);

	parent::init();
    }

    /**
     * Eneueue admin scripts
     */
    public function enqueueScripts() {
	if ( $this->isScreen() ) {
	    wp_enqueue_script( 'mbtheme_layouts_admin_page', MBTHEME_URI_MODULES . '/layouts/js/page.js', array( 'jquery' ), false, true );
	}
	wp_enqueue_script( 'mbtheme_layouts_admin', MBTHEME_URI_MODULES . '/layouts/js/admin.js', array( 'jquery' ), false, true );
    }

    /**
     * Current screen.
     */
    protected function currentScreen() {
	if ( $this->isActiveTab( 'default' ) ) {
	    MBTheme_Layout_Settings_Default::getInstance()->currentTab();
	}
    }

    /**
     * Display default tab.
     */
    protected function tab_default() {
	MBTheme_Layout_Settings_Default::getInstance()->content();
    }

    /**
     * Display sidebars tab.
     */
    protected function tab_sidebars() {
	MBTheme_Sidebars::displaySetting();
    }
    
    /**
     * Display metabox tab.
     */
    protected function tab_metabox() {
	MBTheme_Layouts_Posts::displaySetting();
    }

}
