<?php

final class MBTheme_Sidebars {

    /**
     * @var string Option name, action key, and nonce for default layout settings.
     */
    protected static $key = 'mbtheme_sidebars';

    /**
     * Default left sidebars.
     */
    protected static $default_left = array(
	'id' => 'mbtheme-default-left',
	'name' => 'Default - Left',
	'description' => '',
    );

    /**
     * Default right sidebars.
     */
    protected static $default_right = array(
	'id' => 'mbtheme-default-right',
	'name' => 'Default - Right',
	'description' => '',
    );

    /**
     * Initialization.
     */
    public static function init() {
	// add options if does not exist or not array
	$options = get_option( self::$key );
	if ( !is_array( $options ) ) {
	    update_option( self::$key, array() );
	}

	self::actions();
    }

    /**
     * Actions and Filters.
     */
    private static function actions() {
	// register all sidebars
	add_action( 'widgets_init', array( __CLASS__, 'register' ) );

	// actions callback
	add_action( 'admin_post_' . self::$key . '_add', array( __CLASS__, 'add' ) );
	add_action( 'admin_post_' . self::$key . '_edit', array( __CLASS__, 'edit' ) );
	add_action( 'current_screen', array( __CLASS__, 'delete' ) );
    }

    /**
     * Display setting content for sidebars.
     */
    public static function displaySetting() {
	if ( !isset( $_GET[ 'action' ] ) ) {
	    self::displayList();
	    return;
	}

	switch ( $_GET[ 'action' ] ) {
	    case 'edit_sidebar':
		self::formEdit();
		break;
	    default:
		break;
	}
    }

    /**
     * Display sidebar list and form.
     */
    private static function displayList() {
	?>
	<div id="col-container" class="wp-clearfix">
	    <div id="col-left">
		<div class="col-wrap">
		    <?php
		    self::form();
		    ?>
		</div>
	    </div>
	    <div id="col-right">
		<div class="col-wrap">
		    <?php
		    self::listTable();
		    ?>
		</div>
	    </div>
	</div>
	<?php
    }

    /**
     * Display sidebar form.
     */
    private static function form( $args = array() ) {
	$args = array_merge( array(
	    'title' => _x( 'Add Sidebar', 'Sidebar Settings', 'mbtheme' ),
	    'action' => '_add',
	    'nonce' => '_add',
	    'value_name' => '',
	    'value_description' => '',
	    'value_id' => '',
	    'submit_text' => _x( 'Add New Sidebar', 'Sidebar Settings', 'mbtheme' )
	), $args );
	?>
	<div class="form-wrap">
	    <h2><?php echo $args[ 'title' ]; ?></h2>
	    <form method="POST" action="admin-post.php" class="validate">

		<input type="hidden" name="action" value="<?php echo esc_attr( self::$key . $args[ 'action' ] ); ?>">
		<?php
		wp_nonce_field( self::$key . $args[ 'nonce' ], self::$key . $args[ 'nonce' ] . '_nonce' );
		?>

		<input type="hidden" name="sidebar-id" value="<?php echo esc_attr( $args[ 'value_id' ] ); ?>" />

		<div class="form-field form-required">
		    <label for="sidebar-name"><?php echo __( 'Name', 'mbtheme' ); ?></label>
		    <input name="sidebar-name" id="sidebar-name" type="text" value="<?php echo esc_attr( $args[ 'value_name' ] ); ?>" size="40">
		    <p>Must be unique</p>
		</div>
		<div class="form-field">
		    <label for="sidebar-description"><?php echo __( 'Description', 'mbtheme' ); ?></label>
		    <textarea name="sidebar-description" id="sidebar-description" rows="4"><?php echo esc_html( $args[ 'value_description' ] ); ?></textarea>
		</div>
		<?php
		submit_button( $args[ 'submit_text' ] );
		?>
	    </form>
	</div>
	<?php
    }

    /**
     * Display sidebar list table.
     */
    private static function listTable() {
	$sidebar_list = new MBTheme_Sidebar_List();
	$sidebar_list->prepare_items();
	$sidebar_list->display();
    }

    /**
     * Get sidebar list.
     * 
     * @param string $default Include default (left, right, or all).
     */
    public static function getList( $default = '', $simple = false ) {
	$list = array();

	if ( $default === 'all' || $default === 'left' ) {
	    $list[ self::$default_left[ 'id' ] ] = array(
		'name' => self::$default_left[ 'name' ],
		'description' => self::$default_left[ 'description' ],
	    );
	}
	if ( $default === 'all' || $default === 'right' ) {
	    $list[ self::$default_right[ 'id' ] ] = array(
		'name' => self::$default_right[ 'name' ],
		'description' => self::$default_right[ 'description' ],
	    );
	}

	$sidebars = get_option( self::$key, array() );

	$list = array_merge( $list, $sidebars );

	if ( $simple ) {
	    $list_ = array();
	    foreach ( $list as $key => $value ) {
		$list_[ $key ] = $value[ 'name' ];
	    }
	    $list = $list_;
	}

	return $list;
    }

    /**
     * Add new sidebar by admin-post.
     */
    public static function add() {
	// begin validation
	if ( !current_user_can( 'manage_options' ) ) {
	    wp_die( 'You are not allowed to be on this page.' );
	}
	check_admin_referer( self::$key . '_add', self::$key . '_add_nonce' );
	// end validation

	$process = self::processAdd();
	if ( $process === true ) {
	    add_settings_error( 'general', 'settings_updated', 'Sidebar added.', 'updated' );
	} else {
	    add_settings_error( 'general', 'settings_updated', $process, 'error' );
	}
	set_transient( 'settings_errors', get_settings_errors(), 30 );

	/**
	 * Redirect back to the settings page that was submitted
	 */
	$goback = add_query_arg( 'settings-updated', 'true', wp_get_referer() );
	wp_redirect( $goback );
	exit;
    }

    /**
     * Add sidebar to database.
     * 
     * @return bool|string True or error message 
     */
    private static function processAdd() {

	// filter inputs
	$name = isset( $_POST[ 'sidebar-name' ] ) ? trim( sanitize_text_field( $_POST[ 'sidebar-name' ] ) ) : '';
	$desc = isset( $_POST[ 'sidebar-description' ] ) ? sanitize_text_field( $_POST[ 'sidebar-description' ] ) : '';
	$id = sanitize_title_with_dashes( $name );

	global $wp_registered_sidebars;
	if ( empty( $id ) || isset( $wp_registered_sidebars[ $id ] ) ) {
	    return 'Failed';
	}

	// get current sidebars
	$sidebars = get_option( self::$key, array() );
	// check if sidebar exist
	if ( isset( $sidebars[ $id ] ) ) {
	    return 'Failed';
	}

	// save sidebar
	$sidebars[ $id ] = array(
	    'name' => $name,
	    'description' => $desc,
	);
	if ( !update_option( self::$key, $sidebars ) ) {
	    return 'Failed save option';
	}
	return true;
    }

    /**
     * Display edit sidebar form.
     */
    private static function formEdit() {

	// display back link
	$back_url = MBTheme_Layout_Settings::getInstance()->getUrl( 'sidebars' );
	echo '<p><a href="' . esc_url( $back_url ) . '">Back to sidebar list</a></p>';

	$form_args = array(
	    'title' => _x( 'Edit Sidebar', 'Sidebar Settings', 'mbtheme' ),
	    'action' => '_edit',
	    'nonce' => '_edit',
	    'submit_text' => _x( 'Update Sidebar', 'Sidebar Settings', 'mbtheme' )
	);

	// get current sidebars
	$sidebars = get_option( self::$key, array() );
	$id = isset( $_GET[ 'sidebar_id' ] ) ? $_GET[ 'sidebar_id' ] : '';
	if ( !empty( $id ) && isset( $sidebars[ $id ] ) ) {
	    $form_args[ 'value_name' ] = $sidebars[ $id ][ 'name' ];
	    $form_args[ 'value_description' ] = $sidebars[ $id ][ 'description' ];
	    $form_args[ 'value_id' ] = $id;
	    ?>
	    <div id="col-container" class="wp-clearfix">
	        <div id="col-left">
	    	<div class="col-wrap">
			<?php self::form( $form_args ); ?>
	    	</div>
	        </div>
	    </div>
	    <?php
	} else {
	    echo 'Sidebar does not exist';
	}
    }

    /**
     * Add new sidebar by admin-post.
     */
    public static function edit() {
	// begin validation
	if ( !current_user_can( 'manage_options' ) ) {
	    wp_die( 'You are not allowed to be on this page.' );
	}
	check_admin_referer( self::$key . '_edit', self::$key . '_edit_nonce' );
	// end validation

	$process = self::processEdit();
	if ( $process === true ) {
	    add_settings_error( 'general', 'settings_updated', 'Sidebar updated.', 'updated' );
	} else {
	    add_settings_error( 'general', 'settings_updated', $process, 'error' );
	}
	set_transient( 'settings_errors', get_settings_errors(), 30 );

	/**
	 * Redirect back to the settings page that was submitted
	 */
	$goback = add_query_arg( 'settings-updated', 'true', wp_get_referer() );
	wp_redirect( $goback );
	exit;
    }

    /**
     * Update sidebar to database.
     * 
     * @return bool|string True or error message 
     */
    public static function processEdit() {
	// filter inputs
	$name = isset( $_POST[ 'sidebar-name' ] ) ? trim( sanitize_text_field( $_POST[ 'sidebar-name' ] ) ) : '';
	$desc = isset( $_POST[ 'sidebar-description' ] ) ? sanitize_text_field( $_POST[ 'sidebar-description' ] ) : '';
	$id = isset( $_POST[ 'sidebar-id' ] ) ? trim( sanitize_text_field( $_POST[ 'sidebar-id' ] ) ) : '';

	global $wp_registered_sidebars;
	if ( empty( $id ) ) {
	    return 'Failed';
	}

	// get current sidebars
	$sidebars = get_option( self::$key, array() );
	// save sidebar
	$sidebars[ $id ] = array(
	    'name' => $name,
	    'description' => $desc,
	);
	update_option( self::$key, $sidebars );
	return true;
    }

    /**
     * Delete a sidebar by link.
     */
    public static function delete() {
	// check action
	if ( !MBTheme_Layout_Settings::getInstance()->isScreen( 'delete_sidebar' ) ) {
	    return;
	}
	// begin validation
	if ( !current_user_can( 'manage_options' ) ) {
	    wp_die( 'You are not allowed to be on this page.' );
	}
	if ( !wp_verify_nonce( $_REQUEST[ '_wpnonce' ], 'mbtheme_sidebar_delete_' . $_REQUEST[ 'sidebar_id' ] ) ) {
	    $process = 'Failed nonce';
	} else {
	    $process = self::processDelete();
	}
	// end validation

	if ( $process === true ) {
	    add_settings_error( 'general', 'settings_updated', 'Sidebar deleted.', 'updated' );
	} else {
	    add_settings_error( 'general', 'settings_updated', $process, 'error' );
	}
	set_transient( 'settings_errors', get_settings_errors(), 30 );

	/**
	 * Redirect back to the settings page that was submitted
	 */
	$goback = add_query_arg( 'settings-updated', 'true', wp_get_referer() );
	wp_redirect( $goback );
	exit;
    }

    /**
     * Delete sidebar to database.
     */
    private static function processDelete() {
	$id = isset( $_GET[ 'sidebar_id' ] ) ? trim( sanitize_text_field( $_GET[ 'sidebar_id' ] ) ) : '';
	// get current sidebars
	$sidebars = get_option( self::$key, array() );
	// remove a sidebar
	if ( isset( $sidebars[ $id ] ) ) {
	    unset( $sidebars[ $id ] );
	}
	update_option( self::$key, $sidebars );
	return true;
    }

    /**
     * Register the sidebars.
     */
    public static function register() {
	$default_args = array(
	    'before_widget' => '<div id="%1$s" class="widget %2$s">',
	    'after_widget' => '</div>',
	    'before_title' => '<h2 class="widgettitle">',
	    'after_title' => '</h2>',
	);

	// register listed sidebar
	$sidebars = self::getList( 'all' );
	foreach ( $sidebars as $key => $sidebar ) {
	    register_sidebar( array_merge( array(
		'name' => $sidebar[ 'name' ],
		'id' => $key,
		'description' => $sidebar[ 'description' ],
	    ), $default_args ) );
	}
    }

}
