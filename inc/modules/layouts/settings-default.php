<?php

/**
 * Layout settings controller for default tab.
 */
final class MBTheme_Layout_Settings_Default {

    /**
     * @access private
     * @var	object	Instance of current class
     */
    private static $instance;

    /**
     * @var string Option name, action key, and nonce for default layout settings.
     */
    protected $key = 'mbtheme_layouts';

    /**
     * @var array Object collection for admin form class.
     */
    protected $forms = array();

    /**
     * @var array List of available forms.
     */
    protected $form_list = array();

    /**
     * Private construct because of singleton
     * @access private
     */
    private function __construct() {
	
    }

    /**
     * Private because of singleton
     * @access private
     */
    private function __clone() {
	
    }

    /**
     * Private because of singleton
     * @access private
     */
    private function __wakeup() {
	
    }

    /**
     * Returns instance of this class
     * 
     * @access public
     * @return object instance of this class
     */
    public static function getInstance() {
	if ( self::$instance === null ) {
	    self::$instance = new self();
	    self::$instance->init();
	}
	return self::$instance;
    }

    /**
     * Initialize class
     * 
     * @access protected
     */
    protected function init() {
	// add options if does not exist or not array
	$options = get_option( $this->key );
	if ( !is_array( $options ) ) {
	    update_option( $this->key, array() );
	}

	$this->actions();
    }

    /**
     * Actions and Filters.
     */
    protected function actions() {
	add_action( "admin_init", array( $this, 'setForms' ) );

	// save settings on admin post.
	add_action( "admin_post_{$this->key}", array( $this, 'save' ) );
    }

    /**
     * Set form list and form objects.
     */
    public function setForms() {
	// get public post type
	$post_types = get_post_types( array( 'public' => true ), 'objects' );
	foreach ( $post_types as $type ) {
	    $this->form_list[ 'single_' . $type->name ] = $type->label;
	    if ( $type->name === 'post' || ($type->name !== 'attachment' && $type->has_archive) ) {
		$this->form_list[ 'archive_' . $type->name ] = $type->label . ' (Archive)';
	    }
	}

	$this->form_list[ '_404' ] = '404 Page';
	$this->form_list[ '_search_result' ] = 'Search Result';

	$screen_id = MBTheme_Layout_Settings::getInstance()->getScreenID();

	// create form objects
	$this->forms[ 'default' ] = new MBDL_Admin_Form( '', $screen_id, self::getFields( 'default' ) );
	foreach ( $this->form_list as $id => $title ) {
	    $this->forms[ $id ] = new MBDL_Admin_Form( '', $screen_id, self::getFields( $id ) );
	}
    }

    /**
     * When current tab is default.
     * 
     * current_action hook.
     */
    public function currentTab() {
	add_action( 'admin_print_styles', array( $this, 'printStyles' ), 20 );
    }

    /**
     * Display settings content.
     */
    public function content() {
	?>
	<form method="POST" action="admin-post.php">
	    <div class="metabox-holder">
		<?php
		$this->postbox( 'Default', 'default' );
		foreach ( $this->form_list as $id => $title ) {
		    $this->postbox( $title, $id );
		}
		?>
	    </div>
	    <input type="hidden" name="action" value="<?php echo $this->key; ?>">
	    <?php
	    wp_nonce_field( $this->key, "{$this->key}_nonce" );
	    submit_button( 'Save Settings' );
	    ?>
	</form>
	<?php
    }

    /**
     * Display postbox with fields.
     * 
     * @param string $title	Postbox title.
     * @param string $form_id	Form ID.
     */
    protected function postbox( $title, $form_id ) {
	?>
	<div class="postbox layout-settings-postbox">
	    <h2 class="hndle"><span><?php echo $title; ?></span></h2>
	    <div class="inside">
		<?php $this->displayForm( $form_id ); ?>
	    </div>
	</div>
	<?php
    }

    /**
     * Render all fields
     * 
     * @param string $form_id Form ID.
     */
    protected function displayForm( $form_id ) {
	if ( !isset( $this->forms[ $form_id ] ) ) {
	    return;
	}
	echo '<div class="mbtheme-layouts-fieldset">';
	$this->forms[ $form_id ]->setValues( $this->getValues( $form_id ) );
	$this->forms[ $form_id ]->render();
	echo '</div>';
    }

    /**
     * Action to save the settings.
     */
    public function save() {
	// begin validation
	if ( !current_user_can( 'manage_options' ) ) {
	    wp_die( 'You are not allowed to be on this page.' );
	}
	check_admin_referer( $this->key, "{$this->key}_nonce" );
	// end validation

	/* save settings */
	$options = array();
	if ( isset( $_POST[ 'default' ] ) ) {
	    $options[ 'default' ] = $_POST[ 'default' ];
	}

	foreach ( $this->form_list as $id => $title ) {
	    if ( $id === 'default' ) {
		continue;
	    }
	    // do not save if default is checked
	    if ( isset( $_POST[ $id ][ 'default' ] ) && $_POST[ $id ][ 'default' ] === '1' ) {
		continue;
	    }
	    $options[ $id ] = $_POST[ $id ];
	}

	update_option( $this->key, $options );

	// set error settings
	add_settings_error( 'general', 'settings_updated', 'Settings Updated.', 'updated' );
	set_transient( 'settings_errors', get_settings_errors(), 30 );

	// redirect
	$goback = add_query_arg( 'settings-updated', 'true', wp_get_referer() );
	wp_redirect( $goback );
	exit;
    }

    /**
     * Get layout values by key.
     * 
     * @param string $id Layout key. Default = 'default'.
     */
    public function getValues( $id = 'default' ) {
	$values_ = array(
	    'default' => '1',
	    'container' => 'normal',
	    'sidebar_pos' => 'no_sidebar',
	    'sidebar_left' => 'mbtheme-default-left',
	    'sidebar_right' => 'mbtheme-default-right',
	);
	$values = get_option( $this->key, array() );
	if ( isset( $values[ $id ] ) && is_array( $values[ $id ] ) ) {
	    $values_ = $values[ $id ];
	} elseif ( isset( $values[ 'default' ] ) && is_array( $values[ 'default' ] ) ) {
	    $values_ = $values[ 'default' ];
	}
	return $values_;
    }

    /**
     * Get fieldset for layout settings.
     * 
     * @param string $id ID / Key for field name. Example: id[container].
     * 
     * @return array Fieldset.
     */
    public static function getFields( $id ) {

	$fields = array();

	if ( $id !== 'default' ) {
	    $fields[] = array(
		'id' => 'default',
		'name' => "{$id}[default]",
		'title' => 'Default',
		'type' => 'switch',
		'default' => '1'
	    );
	}

	$fields[] = array(
	    'id' => 'container',
	    'name' => "{$id}[container]",
	    'title' => 'Container',
	    'type' => 'select',
	    'select2' => false,
	    'options' => array(
		'normal' => 'Normal',
		'large' => 'Large',
		'fluid' => 'Fluid',
		'full_width' => 'Full Width',
	    ),
	);
	$fields[] = array(
	    'id' => 'sidebar_pos',
	    'name' => "{$id}[sidebar_pos]",
	    'title' => 'Sidebar Position',
	    'type' => 'select',
	    'select2' => false,
	    'options' => array(
		'no_sidebar' => 'No Sidebar',
		'left' => 'Left',
		'right' => 'Right',
	    ),
	);
	$fields[] = array(
	    'id' => 'sidebar_left',
	    'name' => "{$id}[sidebar_left]",
	    'title' => 'Left Sidebar',
	    'type' => 'select',
	    'select2' => false,
	    'options' => MBTheme_Sidebars::getList( 'left', true ),
	);
	$fields[] = array(
	    'id' => 'sidebar_right',
	    'name' => "{$id}[sidebar_right]",
	    'title' => 'Right Sidebar',
	    'type' => 'select',
	    'select2' => false,
	    'options' => MBTheme_Sidebars::getList( 'right', true ),
	);

	return $fields;
    }

    public function printStyles() {
	?>
	<style>
	    .layout-settings-postbox{
		width: 700px;
		max-width: 100%;
	    }
	    .js .layout-settings-postbox .hndle{
		cursor: default;
	    }
	</style>
	<?php
    }

}
