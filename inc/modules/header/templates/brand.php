<?php
/**
 * Template part for displaying site brand
 */
$classes = (isset( $class )) ? (array) $class : array();

// has description
$description = get_bloginfo( 'description', 'display' );
$has_description = ( $description || is_customize_preview() );

// has custom logo
$has_custom_logo = (function_exists( 'has_custom_logo' ) && has_custom_logo());
if ( $has_custom_logo ) {
    $classes[] = 'has-custom-logo';
} else {
    // has description
    $description = get_bloginfo( 'description', 'display' );
    $has_description = ( $description || is_customize_preview() );
    if ( $has_description ) {
        $classes[] = 'has-description';
    }
}
?>
<div class="site-brand <?php echo esc_attr( implode( ' ', $classes ) ); ?>">
    <?php
    // hide site title and description if has logo
    $hide_title_style = "";
    if ( $has_custom_logo ) {
        the_custom_logo();
        $hide_title_style = 'style="display: none;"';
    }
    if ( is_front_page() && is_home() ) :
        ?>
        <h1 class="site-title" <?php echo $hide_title_style; ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
    <?php else : ?>
        <p class="site-title" <?php echo $hide_title_style; ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
    <?php
    endif;

    // display the site description
    if ( $description || is_customize_preview() ) :
        ?>
        <p class="site-description" <?php echo $hide_title_style; ?>><?php echo $description; ?></p>
    <?php endif;
    ?>
</div><!-- .site-brand -->