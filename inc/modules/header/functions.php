<?php

/**
 * functions for header
 */
if ( !function_exists( 'mbtheme_the_custom_logo' ) ) {

    /**
     * get custom logo
     */
    function mbtheme_the_custom_logo() {
        if ( function_exists( 'the_custom_logo' ) ) {
            echo '<div class="site-logo">';
            the_custom_logo();
            echo '</div>';
        }
    }

}