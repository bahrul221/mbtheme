<?php

class MBTheme_Header_Customize {

    public static function init() {
        add_action( 'customize_register', array( 'MBTheme_Header_Customize', 'register' ) );
    }

    /**
     * Set customizer
     */
    public static function register( $wp_customize ) {
        $wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
        $wp_customize->selective_refresh->add_partial( 'blogname', array(
            'selector' => '.site-title a',
            'render_callback' => array( 'MBTheme_Header_Customize', 'partialBlogname' ),
        ) );

        $wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
        $wp_customize->selective_refresh->add_partial( 'blogdescription', array(
            'selector' => '.site-description',
            'render_callback' => array( 'MBTheme_Header_Customize', 'partialBlogDescription' ),
        ) );

        // set transport of custom logo to refresh
        $wp_customize->get_setting( 'custom_logo' )->transport = 'refresh';
    }

    public static function partialBlogname() {
        bloginfo( 'name' );
    }

    public static function partialBlogDescription() {
        bloginfo( 'description' );
    }

}

MBTheme_Header_Customize::init();
