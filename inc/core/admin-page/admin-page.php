<?php

/**
 * Abstract admin page.
 */
abstract class MBDL_Admin_Page {

    /**
     * @var string The resulting page's hook_suffix
     */
    protected $screen_id;

    /**
     * @var string Main capability for this page
     */
    protected $capability = 'manage_options';

    /**
     * @var array Menu arguments.
     */
    protected $menu_args = array(
	'parent_slug' => '',
	'page_title' => '',
	'menu_title' => '',
	'menu_slug' => '',
	'icon_url' => '',
	'position' => null,
    );

    /**
     * @var array Tab list.
     */
    protected $tabs = array();

    /**
     * @var string The active tab.
     */
    protected $active_tab = '';

    /**
     * Initialization.
     */
    protected function init() {
	// set active tab if available
	$tabs = (array) $this->tabs;
	if ( $tabs ) {
	    reset( $tabs );
	    $this->active_tab = key( $tabs );
	    if ( isset( $_GET[ 'tab' ] ) && $_GET[ 'tab' ] !== '' ) {
		$this->active_tab = $_GET[ 'tab' ];
	    }
	}

	$this->actions();
    }

    /**
     * Set menu arguments.
     * 
     * @param array $args Menu arguments.
     */
    protected function menuArgs( $args ) {
	$this->menu_args = array_merge( $this->menu_args, $args );
    }

    /**
     * Actions and Filters.
     */
    protected function actions() {
	// add menu page.
	add_action( 'admin_menu', array( $this, 'menu' ) );
	// Current screen.
	add_action( 'current_screen', array( $this, '_currentScreen' ) );
	// enqueue admin scripts.
	add_action( 'admin_enqueue_scripts', array( $this, 'enqueueScripts' ) );
    }

    /**
     * Add menu page.
     */
    public function menu() {
	if ( $this->menu_args[ 'parent_slug' ] !== '' ) {
	    $this->screen_id = add_submenu_page( $this->menu_args[ 'parent_slug' ], $this->menu_args[ 'page_title' ], $this->menu_args[ 'menu_title' ], $this->capability, $this->menu_args[ 'menu_slug' ], array( $this, 'content' ) );
	} else {
	    $this->screen_id = add_menu_page( $this->menu_args[ 'page_title' ], $this->menu_args[ 'menu_title' ], $this->capability, $this->menu_args[ 'menu_slug' ], array( $this, 'content' ), $this->menu_args[ 'icon_url' ], $this->menu_args[ 'position' ] );
	}
    }

    /**
     * Current screen action.
     * Call currentScreen method only if this is match screen.
     */
    public function _currentScreen() {
	if ( $this->isScreen() ) {
	    $this->currentScreen();
	}
    }

    /**
     * Current screen action.
     */
    protected function currentScreen() {
	
    }

    /**
     * Display content for this page.
     */
    public function content() {
	?>
	<div class="wrap">
	    <div id="icon-themes" class="icon32"></div>
	    <h2><?php echo $this->menu_args[ 'page_title' ]; ?></h2>
	    <?php
	    settings_errors();
	    $this->contentInner();
	    ?>
	</div>
	<?php
    }

    /**
     * Display tabs.
     */
    protected function tabs() {
	$tabs = (array) $this->tabs;
	$page = $_REQUEST[ 'page' ];
	if ( $tabs ) {
	    ?>
	    <h2 class="nav-tab-wrapper" style="margin-bottom: 20px;">
		<?php
		foreach ( $tabs as $key => $tab ) {
		    $url = esc_url( sprintf( "?page={$page}&tab=%s", $key ) );
		    printf( '<a href="%s" class="nav-tab%s">%s</a>', $url, ($this->active_tab === $key) ? ' nav-tab-active' : '', $tab );
		}
		?>
	    </h2>
	    <?php
	}
    }

    /**
     * Display inner content.
     */
    protected function contentInner() {
	// display tabs.
	$this->tabs();

	// call content according to active tab
	if ( $this->active_tab !== '' ) {
	    $method = 'tab_' . $this->active_tab;
	    if ( method_exists( $this, $method ) ) {
		$this->{$method}();
	    }
	}
    }

    /**
     * Enqueue admin scripts.
     */
    public function enqueueScripts() {
	
    }

    /**
     * Check if current page is this settings page.
     * 
     * @param string $action Action get request.
     * 
     * @return bool
     */
    public function isScreen( $action = '' ) {
	$screen = get_current_screen();
	$is_page = ($screen->id === $this->screen_id);

	if ( $is_page && $action !== '' ) {
	    $is_page = (isset( $_REQUEST[ 'action' ] ) && $_REQUEST[ 'action' ] === $action);
	}

	return $is_page;
    }

    /**
     * Get Screen ID.
     * 
     * @param string $name Description
     */
    public function getScreenID() {
	return $this->screen_id;
    }

    /**
     * Get url of this settings page.
     * 
     * @param string $tab Add tab query args.
     * 
     * @return string
     */
    public function getUrl( $tab = '' ) {
	$path = ($this->menu_args[ 'parent_slug' ] === '') ? 'admin.php' : $this->menu_args[ 'parent_slug' ];
	$url = admin_url( $path );
	// Build edit row action.
	$edit_query_args = array(
	    'page' => $this->menu_args[ 'menu_slug' ],
	);
	if ( !empty( $tab ) ) {
	    $edit_query_args [ 'tab' ] = $tab;
	}
	return add_query_arg( $edit_query_args, $url );
    }

    /**
     * Check for active tab.
     */
    public function isActiveTab( $tab ) {
	return ($tab === $this->active_tab);
    }

    /**
     * Get capability.
     */
    public function getCapability() {
	$this->capability;
    }

}
