<?php

if ( !function_exists( 'mbtheme_customize_preview_js' ) ) {

    function mbtheme_customize_preview_js() {
        wp_enqueue_script( 'mbtheme-customize-preview-js', MBTHEME_URI_ASSETS . '/js/customize-preview.js', array( 'customize-preview' ), false, true );
    }

}
add_action( 'customize_preview_init', 'mbtheme_customize_preview_js' );

if ( !function_exists( 'mbtheme_customize_control_js' ) ) {

    function mbtheme_customize_control_js() {
        wp_enqueue_script( 'mbtheme-customize-controls-js', MBTHEME_URI_ASSETS . '/js/customize-controls.js', array(), false, true );
    }

}
add_action( 'customize_controls_enqueue_scripts', 'mbtheme_customize_control_js' );
