<?php

class MBDL_Post_Meta_Box extends MBDL_Meta_Box {

    /**
     * @var MBDL_Admin_Form Admin Form object.
     */
    protected $form;

    /**
     * @var string Admin Form prefix.
     */
    protected $prefix;

    /**
     * @var array Form fields.
     */
    protected $fields = array();

    /**
     * Constructor.
     * 
     * @see MBDL_Meta_Box::__construct()
     * 
     * @param array $form Array of form with fields, arguments and values.
     * 
     */
    public function __construct( $id, $title, $args = array(), $form = array() ) {
	parent::__construct( $id, $title, $args );

	$args = array_merge( array(
	    'save_as_array' => false,
	), $args );
	$this->args = array_merge( $args, $this->args );
	$this->fields = isset( $form[ 'fields' ] ) ? $form[ 'fields' ] : array();
	$this->prefix = isset( $form[ 'prefix' ] ) ? $form[ 'prefix' ] : $this->id . '_';

	$this->form = new MBDL_Admin_Form( $this->prefix, $this->screen );
	$this->addFields( $this->fields );
    }

    /**
     * Actions and Filters.
     */
    public function actions() {
	parent::actions();

	// add action to save post meta
	add_action( 'save_post', array( $this, 'save' ) );
    }

    /**
     * Display meta box content
     * 
     * @param WP_Post $post Post Object.
     */
    protected function display( $post ) {
	wp_nonce_field( $this->id, $this->id . '_wpnonce', false );
	// render admin form
	$values = $this->getValues( $post->ID );
	$this->form->setValues( $values );
	$this->form->render();
    }

    /**
     * Add fields to meta box.
     * 
     * @see MBDL_Admin_Form:addFields
     * 
     * @param array $fields Array of fields.
     */
    public function addFields( $fields ) {
	// if save as array set name field 
	if ( $this->args[ 'save_as_array' ] ) {
	    foreach ( $fields as $key => $field ) {
		if ( !isset( $field[ 'id' ] ) ) {
		    continue;
		}
		$fields[ $key ][ 'name' ] = $this->id . '[' . $field[ 'id' ] . ']';
	    }
	}

	$this->fields = array_merge( $this->fields, $fields );
	$this->form->addFields( $fields );
    }

    /**
     * Get single value by field id
     * 
     * @param int	$post_id    The Post ID.
     * @param string	$key	    Field ID or Meta key.
     * 
     * @return string Meta Value.
     */
    public function getValue( $post_id, $key ) {
	return get_post_meta( $post_id, $this->prefix . $key, true );
    }

    /**
     * Get all values of meta box
     * 
     * @param int $post_id The Post ID.
     * 
     * @return array All values of meta box
     */
    public function getValues( $post_id ) {
	$value = array();

	// save as array
	if ( $this->args[ 'save_as_array' ] ) {
	    $value = (array) get_post_meta( $post_id, $this->id, true );
	    return $value;
	}

	foreach ( $this->fields as $field ) {
	    if ( !isset( $field[ 'id' ] ) ) {
		continue;
	    }
	    if ( metadata_exists( 'post', $post_id, $this->prefix . $field[ 'id' ] ) ) {
		$value[ $field[ 'id' ] ] = get_post_meta( $post_id, $this->prefix . $field[ 'id' ], true );
	    }
	}
	return $value;
    }

    /**
     * $_POST validation.
     * 
     * @param int $post_id The Post ID.
     */
    protected function validSave( $post_id ) {
	// just for this screen
	$post_type = get_post_type( $post_id );
	// only if screen is set.
	if ( $this->screen && !in_array( $post_type, (array) $this->screen ) ) {
	    return false;
	}

	// return if autosave
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
	    return false;
	}

	// check wpnonce
	if ( !isset( $_POST[ $this->id . '_wpnonce' ] ) || !wp_verify_nonce( $_POST[ $this->id . '_wpnonce' ], $this->id ) ) {
	    return false;
	}

	return true;
    }

    /**
     * Save all fields as post meta
     * 
     * @param int $post_id The Post ID.
     */
    public function save( $post_id ) {
	// validate
	if ( !$this->validSave( $post_id ) ) {
	    return;
	}

	// save as array
	if ( $this->args[ 'save_as_array' ] ) {
	    if ( isset( $_POST[ $this->id ] ) ) {
		update_post_meta( $post_id, $this->id, $_POST[ $this->id ] );
	    } else {
		update_post_meta( $post_id, $this->id, '' );
	    }
	    return;
	}

	//-- Begin - save
	$prefix = $this->prefix;
	// iterate all fields
	foreach ( $this->fields as $field ) {
	    // field id and request from post should exist
	    if ( isset( $field[ 'id' ] ) ) {
		$meta_key = $prefix . $field[ 'id' ];
		if ( isset( $_POST[ $meta_key ] ) ) {
		    $value = $_POST[ $meta_key ];
		    // update post meta
		    update_post_meta( $post_id, $meta_key, $value );
		} else {
		    update_post_meta( $post_id, $meta_key, '' );
		}
	    }
	}
    }

    /**
     * Set order by as meta value and set meta key to query object.
     * 
     * @param array	$column	Column of posts table.
     * @param object	$query	Query object.	
     */
    public function setOrderByColumn( $column, $query ) {
	if ( !isset( $column[ 'meta_key' ] ) ) {
	    return;
	}
	$query->set( 'orderby', 'meta_value' );
	$query->set( 'meta_key', $this->prefix . $column[ 'meta_key' ] );
    }

}
