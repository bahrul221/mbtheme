<?php

/**
 * Custom Post Type
 * 
 */
class MBDL_Post_type {

    /**
     * @access protected
     * 
     * @var string Post type name.
     */
    protected $post_type;

    /**
     * @access protected
     * 
     * @var array Post type args.
     */
    protected $args;

    /**
     * @access protected
     * 
     * @var array Additional options.
     */
    protected $options;

    /**
     * Constructor.
     * 
     * @see	register_post_type()
     * @link	https://codex.wordpress.org/Function_Reference/register_post_type
     * 
     * @param string $post_type	Post type name.
     * @param array $args Post type args.
     * @param array $options Additional Options.
     */
    public function __construct( $post_type, $args = array(), $options = array() ) {
	$this->post_type = $post_type;
	$this->args = $args;

	$default_options = array(
	    'columns' => array()
	);
	$this->options = wp_parse_args( $options, $default_options );

	// filters and actions
	$this->actions();
    }

    /**
     * Actions and Filters.
     */
    protected function actions() {
	// register post type
	add_action( 'init', array( $this, 'register' ) );
	// set the table columns
	add_filter( 'manage_' . $this->post_type . '_posts_columns', array( $this, 'columns' ) );
	add_action( 'manage_' . $this->post_type . '_posts_custom_column', array( $this, 'columnOutput' ), 10, 2 );
	add_filter( 'manage_edit-' . $this->post_type . '_sortable_columns', array( $this, 'sortableColumns' ), 10, 2 );
	// modify query object to sortable column
	add_action( 'pre_get_posts', array( $this, 'setOrderByColumn' ) );
    }

    /**
     * Register post type.
     * 
     * @uses register_post_type()
     */
    public function register() {
	register_post_type( $this->post_type, $this->args );
    }

    /**
     * Get posts.
     * 
     * @param array $args WP Query args.
     * 
     * @link https://codex.wordpress.org/Template_Tags/get_posts
     * 
     * @return array List of posts.
     */
    public function getPosts( $args = array() ) {
	$args [ 'post_type' ] = $this->post_type;
	return get_posts( $args );
    }

    /**
     * Filter the custom columns for post table.
     * 
     * @param array $columns Array of current columns.
     * 
     * @return array Array of table columns name.
     */
    public function columns( $columns ) {
	// remove date column
	unset( $columns[ 'date' ] );

	foreach ( $this->options[ 'columns' ] as $key => $col ) {
	    $columns[ $key ] = $col[ 'title' ];
	}

	// add date column
	$columns[ 'date' ] = __( 'Date' );
	return $columns;
    }

    /**
     * Filter the custom columns that sortable.
     * 
     * @param array $columns Array of current columns.
     * 
     * @return array Array of table columns name.
     */
    public function sortableColumns( $columns ) {
	foreach ( $this->options[ 'columns' ] as $key => $col ) {
	    if ( !isset( $col[ 'type' ] ) ) {
		continue;
	    }
	    // meta_box only
	    if ( $col[ 'type' ] !== 'meta_box' ) {
		continue;
	    }
	    // sortable should true
	    if ( !isset( $col[ 'sortable' ] ) || !$col[ 'sortable' ] ) {
		continue;
	    }
	    $columns[ $key ] = $key;
	}

	return $columns;
    }

    /**
     * Set OrderBy Column query
     * 
     * @param boject $query Query object
     */
    public function setOrderByColumn( $query ) {
	$order_by = $query->get( 'orderby' );
	$post_type = $query->get( 'post_type' );

	// should in admin page, correct post type and main query
	if ( !is_admin() || $post_type !== $this->post_type || !$query->is_main_query() ) {
	    return;
	}
	// get column
	if ( !isset( $this->options[ 'columns' ][ $order_by ] ) ) {
	    return;
	}
	$column = $this->options[ 'columns' ][ $order_by ];

	// type is required
	if ( !isset( $column[ 'type' ] ) ) {
	    return;
	}

	// meta_box type
	if ( $column[ 'type' ] === 'meta_box' && isset( $column[ 'meta_box_id' ] ) ) {
	    MBDL_Posts::setMetaOrderByColumn( $column[ 'meta_box_id' ], $column, $query );
	}

	// TODO: post meta
    }

    /**
     * Output of custom columns.
     * 
     * @param string $column Column key.
     * @param int $post_id The post ID.
     */
    public function columnOutput( $column, $post_id ) {
	if ( !isset( $this->options[ 'columns' ][ $column ] ) ) {
	    return;
	}
	$col = $this->options[ 'columns' ][ $column ];

	echo $this->getColumnOutput( $col, $post_id );
    }

    /**
     * Get the custom column output according to type.
     * 
     * @param array $col Column args.
     * @param int $post_id The post ID.
     * 
     * @return string Custom column output.
     */
    protected function getColumnOutput( $col, $post_id ) {
	$value = '-';
	if ( !isset( $col[ 'type' ] ) ) {
	    return $value;
	}

	// post ID
	if ( $col[ 'type' ] === 'post_id' ) {
	    $value = $post_id;
	}

	// taxonomy
	if ( $col[ 'type' ] === 'taxonomy' && isset( $col[ 'taxonomy' ] ) ) {
	    $term_links = array();
	    $terms = get_the_terms( $post_id, $col[ 'taxonomy' ] );
	    if ( is_array( $terms ) && !is_wp_error( $terms ) ) {
		foreach ( $terms as $term ) {
		    $url = esc_url( get_edit_term_link( $term->term_id, $col[ 'taxonomy' ], $this->post_type ) );
		    $term_links[] = '<a href="' . $url . '">' . $term->name . '</a>';
		}
	    }
	    $value = implode( ', ', $term_links );
	}

	// Meta Box
	if ( $col[ 'type' ] === 'meta_box' && isset( $col[ 'meta_box_id' ] ) && isset( $col[ 'meta_key' ] ) ) {
	    $value = MBDL_Posts::getMetaValue( $col[ 'meta_box_id' ], $post_id, $col[ 'meta_key' ] );
	}

	// TODO: post meta

	return $value;
    }

}
