<?php

/**
 * Custom Post Type
 */
abstract class MBDL_Posts {

    /**
     * @var string Prefix.
     */
    protected static $prefix = 'mb_';

    /**
     * @var array Instances of MBDL_Post_type.
     */
    protected static $post_types = array();

    /**
     * @var array Instances of MBDL_Taxonomy.
     */
    protected static $taxonomies = array();

    /**
     * @var array Instances of MBDL_Post_Meta_Box.
     */
    protected static $meta_boxes = array();

    /**
     * Get prefix.
     * 
     * @return string Prefix.
     */
    public static function getPrefix() {
	return self::$prefix;
    }

    /**
     * Register a custom post type.
     * Add the instance of MBDL_Post_type to self::$post_types list.
     * 
     * @param string	$post_type  Post type name.
     * @param array	$args	    Post type args.
     * @param array	$options    Additional options.
     */
    public static function registerPostType( $post_type, $args = array(), $options = array() ) {
	self::$post_types[ $post_type ] = new MBDL_Post_type( self::$prefix . $post_type, $args, $options );
    }

    /**
     * Register a custom taxonomy.
     * Add the instance of MBDL_Taxonomy to self::$taxonomies list.
     * 
     * @param string	    $name	    Taxonomy name.
     * @param string|array  $object_type    Post type.
     * @param array	    $args	    Taxonomy args.
     * @param array	    $options	    Additional options.
     * 
     */
    public static function registerTaxonomy( $name, $object_type, $args = array(), $options = array() ) {
	self::$taxonomies[ $name ] = new MBDL_Taxonomy( self::$prefix . $name, self::$prefix . $object_type, $args, $options );
    }

    /**
     * Add a meta box.
     * Add the instance of MBDL_Post_Meta_Box to self::$meta_boxes list.
     * 
     * @param string	    $id		Meta Box ID.
     * @param string|array  $post_types Post types that registered by this class.
     * @param array	    $args	Meta Box args.
     * @param array	    $fields	Fields on Meta Box.
     */
    public static function addMetaBox( $id, $title, $post_types, $args = array(), $form = array() ) {
	// screen should exist and array
	if ( !isset( $args[ 'screen' ] ) ) {
	    $args[ 'screen' ] = array();
	} else {
	    $args[ 'screen' ] = (array) $args[ 'screen' ];
	}

	// add prefix to all post types
	if ( $post_types ) {
	    foreach ( (array) $post_types as $type ) {
		$args[ 'screen' ][] = self::$prefix . $type;
	    }
	}
	// initialize meta box
	self::$meta_boxes[ $id ] = new MBDL_Post_Meta_Box( self::$prefix . $id, $title, $args, $form );
    }

    /**
     * Get list of posts.
     * 
     * @param string	$post_type  Post type name that registered by this class.
     * @param array	$args	    WP Query args.
     * 
     * @return array List of Posts.
     */
    public static function getPosts( $post_type, $args = array() ) {
	if ( !isset( self::$post_types[ $post_type ] ) ) {
	    return false;
	}

	return self::$post_types[ $post_type ]->getPosts( $args );
    }

    /**
     * Get a post by post ID.
     * 
     * @param string	$post_type  Post type name that registered by this class.
     * @param array	$post_id    Post ID.
     * 
     * @return array Post.
     */
    public static function getPostByID( $post_type, $post_id ) {
	return self::getPosts( $post_type, array(
	    'include' => $post_id
	) );
    }

    /**
     * Get list of post title.
     * 
     * @param string	$post_type  Post type name that registered by this class.
     * @param array	$args	    WP Query args.
     * 
     * @return array List of post titles with post ID as key
     */
    public static function getPostTitles( $post_type, $args = array() ) {
	$default = array(
	    'numberposts' => -1,
	    'order' => 'ASC',
	    'orderby' => 'title',
	);
	$args = wp_parse_args( $args, $default );

	$titles = array();

	$posts = self::getPosts( $post_type, $args );
	if ( $posts ) {
	    foreach ( $posts as $post ) {
		$titles[ $post->ID ] = $post->post_title;
	    }
	}
	return $titles;
    }

    /**
     * Add fields to meta box.
     * 
     * @see MBDL_Post_Meta_Box::addFields()
     * 
     * @param string $meta_box_id Meta Box ID.
     */
    public static function addMetaFields( $meta_box_id, $fields ) {
	if ( !isset( self::$meta_boxes[ $meta_box_id ] ) ) {
	    return false;
	}

	self::$meta_boxes[ $meta_box_id ]->addFields( $fields );
    }

    /**
     * Get list or single value of meta box fields.
     * 
     * @param string	$meta_box_id	Meta Box ID that registered by this class.
     * @param int	$post_id	Post ID.
     * @param string	$key		Optional. Meta Box key.
     * 
     * @return string|array List or single value of meta box fields.
     */
    public static function getMetaValue( $meta_box_id, $post_id, $key = '' ) {
	if ( !isset( self::$meta_boxes[ $meta_box_id ] ) ) {
	    return false;
	}
	if ( $key === '' ) {
	    $values = self::$meta_boxes[ $meta_box_id ]->getValues( $post_id );
	} else {
	    $values = self::$meta_boxes[ $meta_box_id ]->getValue( $post_id, $key );
	}

	return $values;
    }

    /**
     * Set order by as meta value and set meta key to query object.
     * 	
     * @param string	$meta_box_id	Meta Box ID that registered by this class.
     * @param array	$column		Column of posts table.
     * @param object	$query		Query object.	
     */
    public static function setMetaOrderByColumn( $meta_box_id, $column, $query ) {
	if ( !isset( self::$meta_boxes[ $meta_box_id ] ) ) {
	    return;
	}
	self::$meta_boxes[ $meta_box_id ]->setOrderByColumn( $column, $query );
    }

}
