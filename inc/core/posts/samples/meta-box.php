<?php

$metabox_args = array(
);
$form = array();

$form[ 'fields' ] = array(
    array(
	'id' => 'text',
	'type' => 'text',
	'title' => 'Text',
    ),
    array(
	'id' => 'textarea',
	'type' => 'textarea',
	'title' => 'Textarea',
    ),
    array(
	'id' => 'checkbox',
	'type' => 'checkbox',
	'title' => 'Checkbox',
	'options' => array(
	    'val1' => 'Value 1',
	    'val2' => 'Value 2',
	    'val3' => 'Value 3',
	),
    ),
    array(
	'id' => 'checkbox-single',
	'type' => 'checkbox',
	'title' => 'Single Checkbox',
	'label' => 'checkbox label',
    ),
    array(
	'id' => 'radio-buttons',
	'type' => 'radio',
	'title' => 'Radio Buttons',
	'options' => array(
	    'val1' => 'Value 1',
	    'val2' => 'Value 2',
	    'val3' => 'Value 3',
	),
    ),
    array(
	'id' => 'select',
	'type' => 'select',
	'title' => 'Select',
	'options' => array(
	    'test' => 'Test',
	    'test1' => 'Test1',
	    'test2' => 'Test2',
	),
	'placeholder' => 'Select',
    ),
    array(
	'id' => 'color',
	'type' => 'color',
	'title' => 'Color',
    ),
    array(
	'id' => 'image',
	'type' => 'image',
	'title' => 'Image',
    ),
    array(
	'id' => 'gallery',
	'type' => 'gallery',
	'title' => 'Gallery',
    ),
);

MBDL_Posts::addMetaBox( 'sample', 'Sample Meta Box', 'sample', $metabox_args, $form );
