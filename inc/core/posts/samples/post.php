<?php

/**
 * Sample Custom Post
 */
// register post type
$sample_labels = array(
    'name' => _x( 'Samples', 'post type general name', 'mbtheme' ),
    'singular_name' => _x( 'Sample', 'post type singular name', 'mbtheme' ),
    'menu_name' => _x( 'Samples', 'admin menu', 'mbtheme' ),
    'add_new' => __( 'Add New', 'mbtheme' ),
    'add_new_item' => __( 'Add New Sample', 'mbtheme' ),
    'new_item' => __( 'New Sample', 'mbtheme' ),
    'edit_item' => __( 'Edit Sample', 'mbtheme' ),
    'view_item' => __( 'View Sample', 'mbtheme' ),
    'all_items' => __( 'All Samples', 'mbtheme' ),
    'search_items' => __( 'Search Samples', 'mbtheme' ),
    'parent_item_colon' => __( 'Parent Samples:', 'mbtheme' ),
    'not_found' => __( 'No samples found.', 'mbtheme' ),
    'not_found_in_trash' => __( 'No samples found in Trash.', 'mbtheme' ),
);

// set post args
$sample_args = array(
    'labels' => $sample_labels,
    'public' => true,
    'show_ui' => true,
    'menu_icon' => 'dashicons-admin-post',
    'supports' => array( 'title', 'editor', 'thumbnail' ),
    'rewrite' => array( 'slug' => 'sample' )
);

$sample_options = array();

$sample_options[ 'columns' ] = array(
    'sample_categories' => array(
	'title' => 'Categories',
	'type' => 'taxonomy',
	'taxonomy' => MBDL_Posts::getPrefix() . 'sample_category',
    ),
    'sample_metabox' => array(
	'title' => 'Sample Meta',
	'type' => 'meta_box',
	'meta_box_id' => 'sample',
	'meta_key' => 'text',
	'sortable' => true,
    ),
    'post_id' => array(
	'title' => 'ID',
	'type' => 'post_id',
    ),
);

MBDL_Posts::registerPostType( 'sample', $sample_args, $sample_options );
