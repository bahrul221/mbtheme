<?php

// register taxonomy
$taxonomy_label = array(
	'name'						 => _x( 'Categories', 'taxonomy general name', 'mbtheme' ),
	'singular_name'				 => _x( 'Category', 'taxonomy singular name', 'mbtheme' ),
	'all_items'					 => __( 'All Categories', 'mbtheme' ),
	'edit_item'					 => __( 'Edit Category', 'mbtheme' ),
	'view_item'					 => __( 'View Category', 'mbtheme' ),
	'update_item'				 => __( 'Update Category', 'mbtheme' ),
	'add_new_item'				 => __( 'Add New Category', 'mbtheme' ),
	'new_item_name'				 => __( 'New Category Name', 'mbtheme' ),
	'parent_item'				 => __( 'Parent Category', 'mbtheme' ), // hierarchical 
	'parent_item_colon'			 => __( 'Parent Category:', 'mbtheme' ), // hierarchical 
	'search_items'				 => __( 'Search Categories', 'mbtheme' ),
	'popular_items'				 => __( 'Popular Categories', 'mbtheme' ), // no hierarchical 
	'separate_items_with_commas' => __( 'Separate categories with commas', 'mbtheme' ), // no hierarchical 
	'add_or_remove_items'		 => __( 'Add or remove categories', 'mbtheme' ), // no hierarchical 
	'choose_from_most_used'		 => __( 'Choose from the most used categories', 'mbtheme' ), // no hierarchical 
	'not_found'					 => __( 'No categories found.', 'mbtheme' ), // no hierarchical 
);

$taxonomy_args = array(
	'labels'		 => $taxonomy_label,
	'public'		 => true, // default
	'rewrite'		 => array( 'slug' => 'sample_category' ), // default
	'hierarchical'	 => false // default
);

MBDL_Posts::registerTaxonomy( 'sample_category', 'sample', $taxonomy_args );