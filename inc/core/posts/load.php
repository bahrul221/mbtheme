<?php

$dir = dirname( __FILE__ );
include_once $dir . '/posts.php';
include_once $dir . '/post-type.php';
include_once $dir . '/taxonomy.php';

include_once $dir . '/meta-box.php';
include_once $dir . '/post-meta-box.php';

// samples
include_once $dir . '/samples/post.php';
include_once $dir . '/samples/taxonomy.php';
include_once $dir . '/samples/meta-box.php';