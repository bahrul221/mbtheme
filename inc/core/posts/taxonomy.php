<?php

class MBDL_Taxonomy {

    /**
     * @var string Taxonomy name.
     */
    protected $name;

    /**
     * @var string|array Object type / Post Type.
     */
    protected $object_type;

    /**
     * @var array Taxonomy args.
     */
    protected $args;

    /**
     * @var array Additional Options.
     */
    protected $options;

    /**
     * Constructor.
     * 
     * @param string $name Taxonomy name.
     * @param string|array $object_type	Object type / Post Type.
     * @param array $args Taxonomy args.
     * @param array $options Additional Options.
     */
    public function __construct( $name, $object_type, $args = array(), $options = array() ) {
	$this->name = $name;
	$this->object_type = $object_type;
	$this->args = $args;

	$default_options = array();
	$this->options = wp_parse_args( $options, $default_options );

	$this->actions();
    }

    /**
     * Actions and Filters.
     */
    protected function actions() {
	add_action( 'init', array( $this, 'register' ) );
    }

    /**
     * Register Taxonomy.
     */
    public function register() {
	register_taxonomy( $this->name, $this->object_type, $this->args );
	register_taxonomy_for_object_type( $this->name, $this->object_type );
    }

}
