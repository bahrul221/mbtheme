<?php

/**
 * Meta Box.
 */
class MBDL_Meta_Box {

    /**
     * @var string Meta Box ID.
     */
    protected $id;

    /**
     * @var string Meta Box Title.
     */
    protected $title;

    /**
     * @var string|array Admin screen(s) that will display meta box.
     */
    protected $screen;

    /**
     * @var string Meta Box context.
     */
    protected $context;

    /**
     * Constructor.
     * 
     * @param string $id Meta Box ID.
     * @param string $title Meta Box Title.
     * @param array $args Meta box arguments.
     */
    public function __construct( $id, $title, $args = array() ) {
	$this->id = $id;
	$this->title = $title;
	$this->screen = $args[ 'screen' ];

	$this->args = array_merge( array(
	    'context' => 'advanced',
	    'priority' => 'default',
	    'container_class' => '',
	), $args );

	$this->actions();
    }

    /**
     * Actions and Filters.
     */
    protected function actions() {
	// add meta box
	add_action( 'add_meta_boxes', array( $this, 'add' ) );
    }

    /**
     * Add Meta Box.
     */
    public function add() {
	add_meta_box( $this->id, $this->title, array( $this, '_display' ), $this->screen, $this->args[ 'context' ], $this->args[ 'priority' ] );
    }

    /**
     * Wrap meta box with classes.
     * 
     * @param object $object WP_Post object.
     */
    public function _display( $object ) {
	?>
	<div class="meta-box-<?php echo esc_attr( $this->id ); ?> <?php echo esc_attr( $this->args[ 'container_class' ] ); ?>">
	    <?php $this->display( $object ); ?>
	</div>
	<?php
    }

    /**
     * Display meta box content
     * 
     * @param object $object WP_Post object.
     */
    protected function display( $object ) {
	
    }

}
