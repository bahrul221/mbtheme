<?php

/**
 * Checkbox field
 */
class MBDL_Admin_Field_Checkbox extends MBDL_Admin_Field {

    /**
     * Default arguments for this field.
     */
    protected function getDefaultArgs() {
	return array(
	    'options' => array(),
	    'label' => '',
	);
    }

    /**
     * Display input field.
     */
    public function display( $field ) {
	$field = $this->mergeArgs( $field );
	$values = $field[ 'value' ];
	$atts = 'type="checkbox"';
	$name = esc_attr( $field[ 'name' ] );
	$atts .= ' class="mbdl-af-field-checkbox"';

	if ( $field[ 'options' ] ) {
	    $values = (array) $values;
	    $atts .= ' name="' . $name . '[]"';
	    foreach ( $field[ 'options' ] as $key => $option ) {
		$atts_ = $atts . ' value="' . esc_attr( $key ) . '"';
		$atts_ .= (in_array( $key, $values )) ? ' checked' : '';
		?>
		<label class="mbdl-af-checkbox-option" style="margin-right: 20px;">
		    <input <?php echo $atts_; ?>> <?php echo $option; ?>
		</label>
		<?php
	    }
	} else {
	    $atts .= ' name="' . $name . '"';
	    $atts .= ' value="1"';
	    $atts .= ( $values === '1' ) ? ' checked' : '';
	    ?>
	    <label class="mbdl-af-checkbox-option" style="margin-right: 20px;">
		<input type="hidden" value="" name="<?php echo $name; ?>" />
		<input <?php echo $atts; ?>> <?php echo $field[ 'label' ]; ?>
	    </label>
	    <?php
	}
    }

}
