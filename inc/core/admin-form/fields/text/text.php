<?php

/**
 * Text field
 */
class MBDL_Admin_Field_Text extends MBDL_Admin_Field {

    /**
     * Default arguments for this field
     */
    public function getDefaultArgs() {
	return array(
	    'placeholder' => '',
	);
    }

    /**
     * Display input field
     */
    public function display( $field ) {
	$field = $this->mergeArgs( $field );

	$atts = 'type="text"';
	$atts .= ' name="' . esc_attr( $field[ 'name' ] ) . '"';
	$atts .= ' class="mbdl-af-field regular-text"';
	$atts .= ' placeholder="' . esc_attr( $field[ 'placeholder' ] ) . '"';
	$atts .= ' value="' . esc_attr( $field[ 'value' ] ) . '"';
	?>
	<input <?php echo $atts; ?>>
	<?php
    }

}
