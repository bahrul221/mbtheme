( function ( $ ) {
    $( function( ) {

	$( '.mbdl-af' ).on( 'click', '.mbdl-af-image .mbdl-af-select-image', function () {
	    'use strict';

	    var file_frame;
	    var self = $( this ),
		wrap = self.parent( '.mbdl-af-image' ),
		preview = wrap.find( '.mbdl-af-preview' ),
		image_field = wrap.find( '.mbdl-af-value' ),
		remove_btn = wrap.find( '.mbdl-af-remove-button' );

	    /*
	     * If an instance of file_frame already exists, then open it
	     */
	    if ( undefined !== file_frame ) {
		file_frame.open( );
		return;
	    }

	    // Create a new media frame
	    file_frame = wp.media( {
		title: 'Select Image',
		button: {
		    text: 'Select'
		},
		library: {
		    type: 'image'
		},
		multiple: false
	    } );

	    // select images when open 
	    file_frame.on( 'open', function () {
		var selection = file_frame.state().get( 'selection' );
		var ids = image_field.val().split( ',' );
		ids.forEach( function ( id ) {
		    var attachment = wp.media.attachment( id );
		    attachment.fetch();
		    selection.add( attachment ? [ attachment ] : [ ] );
		} );
	    } );

	    // When an image is selected, run a callback.		    
	    file_frame.on( 'select', function ( ) {

		// We set multiple to false so only get one image from the uploader
		var attachment = file_frame.state().get( 'selection' ).first().toJSON();

		// mush has addtachment id
		if ( !attachment.id ) {
		    return;
		}

		// get image url
		var img_url = attachment.url;
		// get attachment if exist
		if ( undefined !== attachment.sizes.thumbnail ) {
		    img_url = attachment.sizes.thumbnail.url;
		}

		// show image preview
		preview.html( '<a href="' + attachment.url + '" target="_blank"><img src="' + img_url + '" /></a>' );

		// set image field value to attachment ID
		var img_id = attachment.id;
		image_field.val( img_id );

		// show remove button
		remove_btn.show( 0 );

	    } );

	    // Finally, open the modal on click
	    file_frame.open( );

	} );

    } );
}( jQuery ) );