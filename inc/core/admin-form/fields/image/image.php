<?php

/**
 * Image field
 */
class MBDL_Admin_Field_Image extends MBDL_Admin_Field {

    /**
     * Display input field
     */
    public function display( $field ) {
	$field = $this->mergeArgs( $field );
	$value = $field[ 'value' ];
	
	// get image url
	$image_url = wp_get_attachment_image_url( $value );
	$image_url_full = wp_get_attachment_image_url( $value, 'full' );
	// hidden style if no image
	$hidden_style = ( $image_url ) ? '' : 'display: none;';
	// img element
	$image = ($image_url) ? '<a href="' . esc_url( $image_url_full ) . '" target="_blank"><img src="' . esc_url( $image_url ) . '" /></a>' : '';
	?>
	<div class="mbdl-af-field-wrapper mbdl-af-image">
	    <input class="mbdl-af-value" type="hidden" name="<?php echo esc_attr( $field[ 'name' ] ); ?>" value="<?php echo esc_attr( $value ); ?>" />
	    <div class="mbdl-af-preview"><?php echo $image; ?></div>
	    <button type="button" class="mbdl-af-select-image button"><?php _e( 'Select Image', 'mbtheme' ); ?></button>&nbsp;
	    <button type="button" class="button mbdl-af-remove-button mbdl-af-remove-preview" style="<?php echo esc_attr( $hidden_style ); ?>"><?php _e( 'Remove', 'mbtheme' ); ?></button>
	</div>
	<?php
    }

    /**
     * Enqueue scripts
     */
    public function scripts() {
	wp_enqueue_media();
	wp_enqueue_script( 'mbdl-af-field-image-script', parent::getFieldsURL() . '/image/js/script.js', array(), false, true );
    }

}
