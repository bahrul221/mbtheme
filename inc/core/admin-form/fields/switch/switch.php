<?php

/**
 * Checkbox field
 */
class MBDL_Admin_Field_Switch extends MBDL_Admin_Field {

    /**
     * Default arguments for this field.
     */
    protected function getDefaultArgs() {
	return array(
	    'options' => array(),
	    'label' => '',
	);
    }

    /**
     * Display input field.
     */
    public function display( $field ) {
	$field = $this->mergeArgs( $field );
	$values = $field[ 'value' ];
	$atts = 'type="checkbox"';
	$name = esc_attr( $field[ 'name' ] );

	$atts .= ' name="' . $name . '"';
	$atts .= ' value="1"';
	$atts .= ( $values === '1' ) ? ' checked' : '';
	?>
	<label class="mbdl-af-switch">
	    <input type="hidden" value="" name="<?php echo $name; ?>" />
	    <input <?php echo $atts; ?>>
	    <div class="slider"></div>
	</label>
	<?php
    }

}
