<?php

/**
 * Radio field
 */
class MBDL_Admin_Field_Radio extends MBDL_Admin_Field {

    /**
     * Default arguments for this field.
     */
    protected function getDefaultArgs() {
	return array(
	    'options' => array(),
	);
    }

    /**
     * Display input field.
     */
    public function display( $field ) {
	$field = $this->mergeArgs( $field );

	$atts = 'type="radio"';
	$atts .= ' name="' . esc_attr( $field[ 'name' ] ) . '"';
	$atts .= ' class="mbdl-af-field-checkbox"';

	foreach ( $field[ 'options' ] as $key => $option ) {
	    $atts_ = $atts . ' value="' . esc_attr( $key ) . '"';
	    $atts_ .= ($key === $field[ 'value' ] ) ? ' checked' : '';
	    ?>
	    <label class="mbdl-af-radio-buttons-option" style="margin-right: 20px;"><input <?php echo $atts_; ?>> <?php echo $option; ?></label>
	    <?php
	}
    }

}
