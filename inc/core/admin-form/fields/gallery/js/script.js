( function ( $ ) {
    $( function ( ) {

	$( '.mbdl-af' ).on( 'click', '.mbdl-af-gallery .mbdl-af-edit-gallery', function () {
	    'use strict';

	    var self = $( this ),
		wrap = self.parent( '.mbdl-af-gallery' ),
		preview = wrap.find( '.mbdl-af-preview' ),
		gallery_field = wrap.find( '.mbdl-af-value' ),
		remove_btn = wrap.find( '.mbdl-af-remove-button' );

	    // cache gallery settings
	    var gallery_settings = wp.media.view.Settings.Gallery;
	    // hide gallery settings used for posts/pages
	    wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend( {
		template: function ( view ) {
		    return;
		}
	    } );

	    // get current gallery
	    var val = gallery_field.val();
	    var gal_edit = '[gallery ids="0"]';
	    if ( val ) {
		gal_edit = '[gallery ids="' + val + '"]';
	    }

	    // open frame
	    var frame = wp.media.gallery.edit( gal_edit );

	    // when frame closed
	    frame.on( 'close', function () {
		// return gallery settings used for posts/pages
		wp.media.view.Settings.Gallery = gallery_settings;
	    } );

	    // when click Insert Gallery, run callback
	    frame.on( 'update', function () {
		var attachmentIDs = [ ];
		var imageHTML = '';
		var library = frame.state().get( 'library' );

		// iterate selected image
		library.each( function ( image ) {
		    var attachment = image.toJSON();

		    // add attachment id to array
		    attachmentIDs.push( attachment.id );

		    var img_url = attachment.url;
		    // get attachment url if exist
		    if ( undefined !== attachment.sizes.thumbnail ) {
			img_url = attachment.sizes.thumbnail.url;
		    }
		    // image html
		    imageHTML += '<a href="' + attachment.url + '" target="_blank"><img src="' + img_url + '" /></a>';
		} );

		// show image preview
		preview.html( imageHTML );

		// set field value
		attachmentIDs = attachmentIDs.join( "," );
		gallery_field.val( attachmentIDs );

		// show remove button
		if ( attachmentIDs ) {
		    remove_btn.show( 0 );
		}

	    } );

	} );

    } );
}( jQuery ) );