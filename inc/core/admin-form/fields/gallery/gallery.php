<?php

/**
 * Gallery field
 */
class MBDL_Admin_Field_Gallery extends MBDL_Admin_Field {

    /**
     * Display input field
     */
    public function display( $field ) {
	$field = $this->mergeArgs( $field );
	$value = $field[ 'value' ];
	
	$img_html = '';

        // convert IDs to array
        $ids = explode( ',', $value );

        foreach ( $ids as $id ) {
            // get image url
            $image_url = wp_get_attachment_image_url( $id ); // thumbnail
            $image_url_full = wp_get_attachment_image_url( $id, 'full' ); // full
            // add img html
            if ( $image_url ) {
                $img_html .= '<a href="' . esc_url( $image_url_full ) . '" target="_blank"><img src="' . esc_url( $image_url ) . '" /></a>';
            }
        }
        // hidden style if no image
        $hidden_style = ( $img_html ) ? '' : 'display: none;';
	?>
	<div class="mbdl-af-field-wrapper mbdl-af-gallery">
	    <input class="mbdl-af-value" type="hidden" name="<?php echo esc_attr( $field[ 'name' ] ); ?>" value="<?php echo esc_attr( $value ); ?>" />
	    <div class="mbdl-af-preview"><?php echo $img_html; ?></div>
	    <button type="button" class="mbdl-af-edit-gallery button"><?php _e( 'Add/Edit Gallery', 'mbtheme' ); ?></button>&nbsp;
	    <button type="button" class="button mbdl-af-remove-button mbdl-af-remove-preview" style="<?php echo esc_attr( $hidden_style ); ?>"><?php _e( 'Remove', 'mbtheme' ); ?></button>
	</div>
	<?php
    }

    /**
     * Enqueue scripts
     */
    public function scripts() {
	wp_enqueue_media();
	wp_enqueue_script( 'mbdl-af-field-gallery-script', parent::getFieldsURL() . '/gallery/js/script.js', array(), false, true );
    }

}
