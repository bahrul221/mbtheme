( function ( $ ) {
    $( function ( ) {

	if ( $.fn.select2 === undefined ) {
	    // marquee is loaded and available
	    return;
	}

	$( '.mbdl-af-field-select2' ).select2( {
	    minimumResultsForSearch: 'Infinity',
	    placeholder: $( this ).data( 'placeholder' ),
	    allowClear: $( this ).data( 'allow-clear' )
	} );

    } );
}( jQuery ) );