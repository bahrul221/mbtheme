<?php

/**
 * Select field
 */
class MBDL_Admin_Field_Select extends MBDL_Admin_Field {

    /**
     * Default arguments for this field
     */
    public function getDefaultArgs() {
	return array(
	    'multiple' => false,
	    'options' => array(),
	    'select2' => true,
	    'placeholder' => '',
	    'allow_clear' => false,
	);
    }

    /**
     * Display input field
     */
    public function display( $field ) {
	$field = $this->mergeArgs( $field );
	$value = (array) $field[ 'value' ];

	$name = $field[ 'name' ];
	$name .= ($field[ 'multiple' ]) ? '[]' : '';
	$atts = 'name="' . esc_attr( $name ) . '"';

	$class = 'mbdl-af-field mbdl-af-field-select regular-text';
	$class .= ($field[ 'select2' ]) ? ' mbdl-af-field-select2' : '';
	$atts .= ' class="' . $class . '"';
	$atts .= ($field[ 'multiple' ]) ? ' multiple' : '';

	$atts .= ' data-placeholder="' . $field[ 'placeholder' ] . '"';
	$atts .= ' data-allow-clear="' . $field[ 'allow_clear' ] . '"';
	?>
	<select <?php echo $atts; ?>>
	    <?php
	    // add empty option if allow clear
	    if ( $field[ 'allow_clear' ] ) {
		// add placeholder text if without select2
		$placeholder = (!$field[ 'select2' ]) ? $field[ 'placeholder' ] : '';
		echo '<option>' . $placeholder . '</option>';
	    }
	    foreach ( $field[ 'options' ] as $key => $option ) {
		$selected = ( in_array( $key, $value )) ? ' selected' : '';
		echo '<option' . $selected . ' value="' . esc_attr( $key ) . '">' . esc_html( $option ) . '</option>';
	    }
	    ?>
	</select>
	<?php
    }

    /**
     * Enqueue scripts
     */
    public function scripts() {
	wp_enqueue_style( 'select2-css', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css' );

	wp_enqueue_script( 'select2-js', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js', array( 'jquery' ), '4.0.3', true );
	wp_enqueue_script( 'mbdl-af-field-select-script', parent::getFieldsURL() . '/select/js/script.js', array( 'select2-js' ), false, true );
    }

}
