<?php

/**
 * Textarea field
 */
class MBDL_Admin_Field_Textarea extends MBDL_Admin_Field {

    /**
     * Default arguments for this field
     */
    public function getDefaultArgs() {
	return array(
	    'placeholder' => '',
	    'rows' => 4,
	);
    }

    /**
     * Display input field
     */
    public function display( $field = array() ) {
	$field = $this->mergeArgs( $field );
	
	$atts = 'class="mbdl-af-field widefat"';
	$atts .= ' name="' . esc_attr( $field[ 'name' ] ) . '"';
	$atts .= ' placeholder="' . esc_attr( $field[ 'placeholder' ] ) . '"';
	$atts .= ' rows="' . esc_attr( $field[ 'rows' ] ) . '"';
	?>
	<textarea <?php echo $atts; ?>><?php echo esc_html( $field[ 'value' ] ); ?></textarea>
	<?php
    }

}
