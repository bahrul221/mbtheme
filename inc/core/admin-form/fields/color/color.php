<?php

/**
 * Color field
 */
class MBDL_Admin_Field_Color extends MBDL_Admin_Field {

    /**
     * Display input field
     */
    public function display( $field ) {
	$field = $this->mergeArgs( $field );

	$atts = 'type="text"';
	$atts .= ' name="' . esc_attr( $field[ 'name' ] ) . '"';
	$atts .= ' class="mbdl-af-field-color widefat"';
	$atts .= ' value="' . esc_attr( $field[ 'value' ] ) . '"';
	?>
	<input <?php echo $atts; ?>>
	<?php
    }

    /**
     * Enqueue scripts
     */
    public function scripts() {
	wp_enqueue_style( 'wp-color-picker' );
	wp_enqueue_script( 'mbdl-af-field-color-script', parent::getFieldsURL() . '/color/js/script.js', array( 'wp-color-picker' ), false, true );
    }

}
