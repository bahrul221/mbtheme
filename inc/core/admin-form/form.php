<?php

/**
 * Admin Form
 */
class MBDL_Admin_Form {

    /**
     * @var array Collection of field type objects.
     */
    protected $field_types = array();

    /**
     * @var array Collection of unregistered fields.
     */
    protected $fields_ = array();

    /**
     * @var array Collection of fields.
     */
    protected $fields = array();

    /**
     * @var array Collection of fields.
     */
    protected $values = array();

    /**
     * @var string prefix of fields.
     */
    protected $prefix = '';

    /**
     * @var string Screen ID.
     */
    protected $screen_id = '';

    /**
     * @var	array Form options.
     */
    protected $options = array();

    /**
     * Constructor.
     * 
     * @param array	    $prefix Prefix of fields.
     * @param string|array  $screen_id Screen ID.
     * @param array	    $fields List of fields.
     * @param array	    $values List of field values.
     * @param array	    $options An array for form option.
     */
    public function __construct( $prefix = '', $screen_id = null, $fields = array(), $values = array(), $options = array() ) {
	$this->prefix = $prefix;
	$this->screen_id = $screen_id;
	$this->values = (array) $values;
	$this->options = $options;

	$this->addFields( (array) $fields );

	$this->actions();
    }

    /**
     * Actions and filters.
     */
    protected function actions() {
	add_action( 'admin_enqueue_scripts', array( $this, 'scripts' ) );

	add_action( 'current_screen', array( $this, 'registerFields' ) );
    }

    /**
     * Enqueue admin scripts and styles.
     */
    public function scripts() {
	wp_enqueue_style( 'mbdl-af-style', MBTHEME_URI_CORE . '/admin-form/assets/css/admin-form.css' );
	wp_enqueue_script( 'mbdl-af-script', MBTHEME_URI_CORE . '/admin-form/assets/js/admin-form.js', array( 'jquery' ), false, true );
    }

    /**
     * Load field type class and collect the object of field type class.
     * 
     * @param array $field Single field args.
     * 
     * @return bool True if object is created.
     */
    private function load( $field ) {
	$type = $field[ 'type' ];
	$class = 'MBDL_Admin_Field_' . ucwords( str_replace( '-', '_', $type ), '_' );
	$path = dirname( __FILE__ ) . "/fields/$type/$type.php";

	if ( !file_exists( $path ) ) {
	    return false;
	}
	include_once $path;

	if ( !class_exists( $class ) ) {
	    return false;
	}
	// cache objects of field type
	$this->field_types[ $type ] = new $class( $this->prefix );

	return true;
    }

    /**
     * Includes all registered fields.
     */
    public function registerFields() {
	if ( !$this->isCurrentScreen() ) {
	    return;
	}

	$fields_ = $this->fields_;
	$fields = array();
	foreach ( $fields_ as $field ) {
	    // add field to list is field type object was created
	    if ( isset( $this->field_types[ $field[ 'type' ] ] ) ) {
		$fields[ $field[ 'id' ] ] = $field;
	    } elseif ( $this->load( $field ) ) {
		$fields[ $field[ 'id' ] ] = $field;
	    }
	}
	$this->fields = $fields;
    }

    /**
     * Get list of fields.
     * 
     * @return array List of fields.
     */
    public function getFields() {
	return $this->fields;
    }

    /**
     * Add fields.
     * 
     * @param array $fields Fields, id and type should exist.
     */
    public function addFields( $fields = array() ) {
	$fields_ = $this->fields_;
	foreach ( $fields as $field ) {
	    // field id and type should exist
	    if ( !isset( $field[ 'id' ] ) || !isset( $field[ 'type' ] ) ) {
		continue;
	    }
	    $fields_[ $field[ 'id' ] ] = $field;
	}
	$this->fields_ = $fields_;
    }

    /**
     * Set field values
     * 
     * @param array $values List of values with array key as field ID.
     */
    public function setValues( $values = array() ) {
	$this->values = (array) $values;
    }

    /**
     * Render all fields
     */
    public function render() {
	if ( !is_array( $this->fields ) ) {
	    return;
	}
	?>
	<div class="mbdl-af">
	    <table class="form-table">
		<tbody>
		    <?php foreach ( $this->fields as $key => $field ) : ?>
	    	    <tr class="mbdl-af-group">
	    		<th scope="row"><label><?php echo (isset( $field[ 'title' ] )) ? $field[ 'title' ] : ''; ?></label></th>
	    		<td>
				<?php
				if ( isset( $this->values[ $key ] ) ) {
				    $field[ 'value' ] = $this->values[ $key ];
				} else {
				    // unset value so default key will be used
				    unset( $field[ 'value' ] );
				}
				$this->field_types[ $field[ 'type' ] ]->display( $field );
				?>
	    		</td>
	    	    </tr>
		    <?php endforeach; ?>
		</tbody>
	    </table>
	</div>
	<?php
    }

    /**
     * Check if current screen will display this form.
     * 
     * @return bool True if this screen.
     */
    public function isCurrentScreen() {
	$is_screen = false;
	if ( empty( $this->screen_id ) ) {
	    $is_screen = true;
	} else {
	    $screen = get_current_screen();
	    if ( is_string( $this->screen_id ) ) {
		$is_screen = ($screen->id === $this->screen_id);
	    } elseif ( is_array( $this->screen_id ) ) {
		$is_screen = in_array( $screen->id, $this->screen_id );
	    }
	}
	return $is_screen;
    }

}
