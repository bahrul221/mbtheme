( function ( $ ) {
    $( document ).ready( function () {
	
	// remove input value and preview
        $( '.mbdl-af' ).on( 'click', '.mbdl-af-field-wrapper .mbdl-af-remove-button', function() {
            var self = $( this ),
                    wrap = self.parent( '.mbdl-af-field-wrapper' ),
                    preview = wrap.find( '.mbdl-af-preview' ),
                    field = wrap.find( '.mbdl-af-value' ),
                    remove_btn = wrap.find( '.mbdl-af-remove-button' );

            // remove gallery preview
            preview.html( '' );

            // remove input value
            field.val( '' );

            // hide button if has preview
            if ( $( this ).hasClass( 'mbdl-af-remove-preview' ) ) {
                remove_btn.hide( 0 );
            }
        } );
	
    } );
}( jQuery ) );