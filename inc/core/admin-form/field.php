<?php

/**
 * Abstract field class
 */
abstract class MBDL_Admin_Field {

    protected $prefix = '';

    /**
     * Constructor.
     * 
     * @param string $prefix Prefix for field.
     */
    public function __construct( $prefix = '' ) {
	$this->prefix = $prefix;

	// enqueue scripts
	add_action( 'admin_enqueue_scripts', array( $this, 'scripts' ) );
    }

    /**
     * Merge default arguments.
     * Set name, default, and value.
     * 
     * @param array $args Field arguments that will be merged.
     * 
     * @return array Merged arguments.
     */
    protected function mergeArgs( $args = array() ) {
	// ID is required.
	if ( !isset( $args[ 'id' ] ) ) {
	    $args[ 'id' ] = '';
	}
	// merge with default args from field.
	$merged_args = array_merge( array(
	    'default' => '',
	    'name' => $this->prefix . $args[ 'id' ],
	), (array) $this->getDefaultArgs() );

	$merged_args = array_merge( $merged_args, $args );

	// set value if exist or default.
	$merged_args[ 'value' ] = (isset( $merged_args[ 'value' ] )) ? $merged_args[ 'value' ] : $merged_args[ 'default' ];

	return $merged_args;
    }

    /**
     * Default arguments for this field
     */
    protected function getDefaultArgs() {
	return array();
    }

    /**
     * Display input field.
     * 
     * @param array $args Field arguments.
     */
    abstract public function display( $field );

    /**
     * Enqueue scripts
     */
    public function scripts() {
	
    }

    public static function getFieldsURL() {
	return MBTHEME_URI_CORE . '/admin-form/fields';
    }

}
